#  1、弹性布局（Flexbox）

## 1.1、介绍

Flexbox 是 flexible box 的简称（注：意思是“灵活的盒子容器”），是 CSS3 引入的新的布局模式。它决定了元素如何在页面上排列，使它们能在不同的屏幕尺寸和设备下可预测地展现出来

**它之所以被称为 Flexbox ，是因为它能够扩展和收缩 flex 容器内的元素，以最大限度地填充可用空间**。与以前布局方式（如 table 布局和浮动元素内嵌块元素）相比，Flexbox 是一个更强大的方式：

- **在不同方向排列元素**

- **重新排列元素的显示顺序**

- **更改元素的对齐方式**

- **动态地将元素装入容器**

采用 Flex 布局的元素，称为 Flex 容器（flex container），简称"**容器**"。它的所有子元素自动成为容器成员，称为 Flex 项目（flex item），简称"**项目**"

## 1.2、简单使用

只要给父元素的添加 **display: flex;** 属性，就可以将元素改变为弹性容器，里面的子元素会自动横向排列。如果元素过多可能会换行，就需要给容器加上**flex-wrap: wrap;**属性让其在合适的时候换行


## 1.3、容器的属性（父元素）

![img](Bootstrap笔记.assets/1287814-20190227105554256-71254015.png)

### 1.3.1、flex-wrap属性  项目元素换行方式      --常用

- nowrap（默认）：不换行。
- **wrap：换行，第一行在上方。** --常用
- wrap-reverse：换行，第一行在下方。

### 1.3.2、 flex-direction  项目元素排列方式

- row（默认值）：主轴为水平方向，起点在左端。
- row-reverse：主轴为水平方向，起点在右端。
- column：主轴为垂直方向，起点在上沿。
- column-reverse：主轴为垂直方向，起点在下沿。
![img](Bootstrap笔记.assets/1287814-20190227112100971-1704943994.png)

### 1.3.3、justify-content   水平对齐方式   --常用

- flex-start（默认值）：左对齐

- flex-end：右对齐

- center： 居中

- space-between：两端对齐，项目之间的间隔都相等。

- space-around：每个项目两侧的间隔相等。所以，项目之间的间隔比项目与边框的间隔大一倍。  --常用

  **注意，项目垂直排列时，此属性设置垂直对齐方式**
  
  ![img](Bootstrap笔记.assets/1287814-20190227115738642-2112201313.gif)

### 1.3.4、align-items    垂直对齐方式 --常用

- flex-start：交叉轴的起点对齐。

- flex-end：交叉轴的终点对齐。

- center：交叉轴的中点对齐。

- baseline: 项目的第一行文字的基线对齐。

- stretch（默认值）：如果项目未设置高度或设为auto，将占满整个容器的高度。

  **注意，项目垂直排列时，此属性设置水平对齐方式**

![img](Bootstrap笔记.assets/1287814-20190227134053190-1350217843.gif)

## 1.4、项目的属性（子元素）

![img](Bootstrap笔记.assets/1287814-20190227135037726-1170549985.png)

### 1.4.1、 order   排列顺序

值为具体的整数，可以为负数，越小越靠前，一般给需要调整顺序的个别元素单独设置

### 1.4.2、flex-grow  放大比例  --常用

 按比例缩放子元素宽度，以便填充满整个父元素。   默认值为0，为0时不放大，可能填不满父元素

### 1.4.3、flex-basis  子元素初始长度  --常用

默认值是 auto ，如果设置了具体的长度，那width值将失效

### 1.4.4、align-self  项目单独的垂直对齐方式

默认值为 **auto**  效果与父元素的align-items属性相同，但只改变子元素自己

### 1.4.5、 flex    放大缩小属性简写，一般只设置放大属性的值   --常用

用法：flex: flex-grow flex-shrink flex-basis  如：flex：1 1 100px;  

经常简写为   flex：1 ；

### 1.4.6、flex：1 和  flex-grow: 1  的区别  --易混淆

flex：1； 实际设置了三个值：flex-grow=1，flex-shrink=1，flex-basis=0%

flex-grow: 1；只设置了一个值：flex-grow=1，flex-shrink=1（默认值），flex-basis=auto（默认值）

所以 设置了  flex：1；之后，子元素的width属性会不起作用

# 2、媒体查询

## 2.1、 介绍

为了更好的适配不同大小的屏幕，我们需要在不同分辨率下进行不同的样式设置，媒体查询功能就是设置多套样式方案，在不同的显示环境下启用对应的方案，也是响应式布局的基础

```html
<!-- 如果要在移动端展示web页面，那就要在元标签（meta）中设置视口缩放比例-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```



## 2.2、 语法(内部样式表)   --常用

```css
@media [only|not] 设备类型 [and (限制条件)] {
    css选择器{
        属性:值;
        ......
    }
}

/* 实现常见的导航菜单小屏隐藏大屏显示 */
.menu{
    display: flex;
}
.btn{
    display: none;
}
@media screen and (max-width: 768px) {
    .btn{
        display: block;
    }
    .menu{
        display: none;
        flex-direction: column;
    }
    .top:hover .menu{
        display: flex;
    }
}
```

## 2.3、 语法(外部样式表)

```html
<link  rel="stylesheet"  href="style1.css"  media=" [only|not] 设备类型 [and (限制条件)]" />
```



## 2.3、 语法格式细节  --重点

- 关键字前后必须有空格 

- and后面的条件要放小括号里面 ，条件是css的属性和值，用冒号隔开，不用写分号

- 可以跟多个条件，但是要注意顺序

- 媒体查询的代码最好放在最后，否则部分效果可能不生效

## 2.4、 设备类型

  ![image-20220904191552518](Bootstrap笔记.assets/image-20220904191552518.png)

## 2.5 关键字

![image-20220904191923695](Bootstrap笔记.assets/image-20220904191923695.png)

# 3、Bootstrap入门

## 3.1、介绍

Bootstrap 是基于HTML、CSS、JavaScript 开发的简洁、直观、强悍的前端开发框架，底层采用Flex和媒体查询布局，使得 Web 开发更加快捷。

内置了很多样式，只要在标签中添加对应的类名即可，使用方便，样式美观，如添加“ btn btn-primary”类名，就可以得到一个按钮

底层依赖JQuery，因此需要先引入Jquery文件

整合了很多常用组件，让开发一些复杂功能变的简单，如轮播图

从v3.0开始支持移动优先，目前主流是v4.0，最新的是v5.0

## 3.2、 使用

官方文档地址：https://v4.bootcss.com/docs/getting-started/introduction/

官网部分页面没有翻译为中文，建议用谷歌浏览器自带的页面翻译进行阅读学习

需要在页面引入的文件：

![image-20220914113852751](Bootstrap笔记.assets/image-20220914113852751.png)

```HTML
<link rel="stylesheet" href="../Bootstrap4.6.1/css/bootstrap.css">
<!-- 先引入jquery 再引入bootstrap的js文件 -->
<script src="../Bootstrap4.6.1/js/jquery.js"></script>
<script src="../Bootstrap4.6.1/js/bootstrap.bundle.js"></script>
```

```html
<body>
    <div class="btn btn-success">div按钮</div>
    <a href="" class="btn btn-primary">链接按钮</a>
</body>
```

# 4、Bootstrap布局

## 4.1、 容器 （.container） --了解

![image-20220914114907132](Bootstrap笔记.assets/image-20220914114907132.png)

## 4.2、栅格系统   --重要

### 4.2.1、 认识栅格系统

采用行+列布局，**.row** 定义行， **.col** 定义列，列要放在行中

每行默认分为12列，可以定义指定列所占的列数或偏移量，默认宽度是按百分比设置

**每一列默认左右内边距15px**  

每行默认左右外边距-15px，用以抵消.container的内边距

### 4.2.2、 响应式栅格布局

可以在同一列指定不同屏幕下所占列数，需要加前缀：
![image-20220914164533230](Bootstrap笔记.assets/image-20220914164533230.png)

### 4.2.3、其他类的效果

.col  --自动填充宽度

.col-md-auto  --根据内容设置宽度

.order-first、.order-last、.order-* --指定列的顺序（排序）

.offset-md-*    --偏移列，改变列的水平位置

## 4.3、对齐方式	--熟悉

### 4.3.1、垂直对齐

 .row + .align-items-start/center/end    -- 整行的垂直对齐方式

.col +  .align-self-start/center/end		--指定列的对齐方式

### 4.3.2、 水平对齐方式

.row +  .justify-content-start/center/end/around/between

# 5、 全局样式（工具类）--熟悉

## 5.1、 边框

### 5.1.1、 添加边框

```html
<span class="border"></span>
<span class="border-top"></span>
<span class="border-right"></span>
<span class="border-bottom"></span>
<span class="border-left"></span>
```

### 5.1.2、 取掉边框

```html
<span class="border-0"></span>
<span class="border-top-0"></span>
<span class="border-right-0"></span>
<span class="border-bottom-0"></span>
<span class="border-left-0"></span>
```

### 5.1.3、 带颜色的边框

```html
<span class="border border-primary"></span>
<span class="border border-secondary"></span>
<span class="border border-success"></span>
<span class="border border-danger"></span>
<span class="border border-warning"></span>
<span class="border border-info"></span>
<span class="border border-light"></span>
<span class="border border-dark"></span>
<span class="border border-white"></span>
```

### 5.1.4、 圆角边框

![image-20220914205058734](Bootstrap笔记.assets/image-20220914205058734.png)

![image-20220914205114770](Bootstrap笔记.assets/image-20220914205114770.png)

## 5.2、背景色和文字颜色

##  5.3、 内外边界

![image-20220914205814103](Bootstrap笔记.assets/image-20220914205814103.png)

.mx-auto	--水平居中

.mx-md-n*    --负外边距和上面的外边距对应，`margin`属性可以使用负值（`padding`不能）经常用以抵消父元素的padding 

## 5.4、 尺寸

## 5.5、 浮动

## 5.6、display 属性

## 5.7、交互--文本选择

## 5.8、溢出与定位

## 5.9、文本

## 5.10、垂直对齐--注意适用元素

## 5.11、 阴影与可见性

## 6.3、 导航栏    --重点

```html
<!-- 
				navbar： 导航栏基本样式；
				navbar-expand-lg：在什么时候显示导航菜单，lg在大屏以上显示
				navbar-light：导航栏配色	
				bg-light：导航栏背景色	，配色和背景色颜色单词要一致						  
		-->
		<nav class="navbar navbar-expand-md navbar-dark bg-dark">
			<!-- 导航栏标题  不许要的时候可以删 -->
		  <a class="navbar-brand" href="#">导航栏</a>
		  <!-- 小屏时显示的菜单按钮 -->
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		<!-- 导航菜单，小屏时隐藏 -->
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#">首页</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">产品</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">产品</a>
		      </li>
			  <li class="nav-item">
			    <a class="nav-link" href="#">产品</a>
			  </li>
		      <li class="nav-item">
		        <a class="nav-link ">关于我们</a>
		      </li>
		    </ul>
			<!-- 表单--搜索框  需要哪个留哪个-->
		    <form class="form-inline my-2 my-lg-0">
		      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		    </form>
		  </div>
		</nav>
```

![image-20220927105310699](Bootstrap笔记.assets/image-20220927105310699.png)

![image-20220927105332532](Bootstrap笔记.assets/image-20220927105332532.png)

### 6.4、 导航栏分析  --- 重点

![image-20220927112249570](Bootstrap笔记.assets/image-20220927112249570.png)

![image-20220927112524467](Bootstrap笔记.assets/image-20220927112524467.png)

![image-20220927113444676](Bootstrap笔记.assets/image-20220927113444676.png)


# 7、轮播图组件

```html
<body>
		<!-- 轮播图组件
			data-interva: 轮播图切换的时间，单位毫秒
		 -->
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="500">
			<!-- 轮播图组件下方的横条切换按钮 
					data-slide-to : 指定点击后切换到第几个图片，从0开始
					data-target:	指向轮播组件的id
			
			-->
		  <ol class="carousel-indicators">
		    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  </ol>
		  <!-- 轮播的内容 -->
		  <div class="carousel-inner">
			  <!-- 单次显示的内容 -->
		    <div class="carousel-item active">
		      <img src="./img/img1.jpg" class="d-block w-100" alt="...">
		    </div>
		    <div class="carousel-item">
		      <img src="./img/img2.jpg" class="d-block w-100" alt="...">
		    </div>
		    <div class="carousel-item">
		      <img src="./img/img3.jpg" class="d-block w-100" alt="...">
		    </div>
		  </div>
		  <!-- 轮播组件的左侧按钮 -->
		  <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </button>
		   <!-- 轮播组件的右侧按钮 -->
		  <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </button>
		</div>
	</body>
```

# 8、卡片

## 8.1、 基本使用

```html
<!-- 卡片组件 父元素有默认的宽带（行内样式），大部分情况我们都需要删掉这个样式 -->
<div class="card" >
    <!-- 卡片顶部的图片  card-img-top  根据需要设置图片的大小-->
    <img src="./img/img1.jpg" class="card-img-top" alt="...">
    <!-- 卡片内容部分 -->
    <div class="card-body">
        <!-- 内容标题 -->
        <h5 class="card-title">Card title</h5>
        <!-- 内容文字 -->
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <!-- 链接按钮 -->
        <a href="#" class="btn btn-primary">Go somewhere</a>
    </div>
</div>
```

## 8.2、列表

![image-20220930093802913](C:\Users\sld\AppData\Roaming\Typora\typora-user-images\image-20220930093802913.png)

```html
<div class="card" style="width: 18rem;">
    <img src="./img/2.jpg" class="card-img-top" alt="...">
    <!-- 第一个卡片内容部分 -->
    <div class="card-body">
        <h5 class="card-title">标题</h5>
        <p class="card-text">卡片内容卡片内容卡片内容卡片内容卡片内容卡片内容卡片内容卡片内容</p>
    </div>
    <!-- 卡片列表 -->
    <!-- 卡片列表不要放到 .card-body里面去-->
    <ul class="list-group list-group-flush">
        <li class="list-group-item">An item</li>
        <li class="list-group-item">A second item</li>
        <li class="list-group-item">A third item</li>
    </ul>
    <!-- 第二个卡片内容部分 -->
    <div class="card-body">
        <a href="#" class="card-link">Card link</a>
        <a href="#" class="card-link">Another link</a>
    </div>
</div>
```

# 9、微票儿

## 9.1、导航栏

![image-20221012152033902](Bootstrap笔记.assets/image-20221012152033902.png)

![image-20221012152157684](Bootstrap笔记.assets/image-20221012152157684.png)

## 9.2、轮播图和特惠看

![image-20221012161138528](Bootstrap笔记.assets/image-20221012161138528.png)



![image-20221012161432304](Bootstrap笔记.assets/image-20221012161432304.png)

![image-20221012172054074](Bootstrap笔记.assets/image-20221012172054074.png)

## 9.3、热播

### 9.3.1 选项卡

![image-20221012162709276](Bootstrap笔记.assets/image-20221012162709276.png)

![image-20221012163040654](Bootstrap笔记.assets/image-20221012163040654.png)

### 9.3.2、技能课模块

```html
<!-- 技能课选项卡 -->
<div class="col-12 col-md-3">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
               aria-controls="home" aria-selected="true">首页</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
               aria-controls="profile" aria-selected="false">技能课</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="card border-0">
                <img src="./img/img2.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">[哈尔滨]梁静茹.你的名字是爱情演唱会</p>
                    <p class="card-text">[太原]蔡依林2016PLAY世界巡回演唱会</p>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="card">
                <img src="./img/img3.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">[哈尔滨]梁静茹.你的名字是爱情演唱会</p>
                    <p class="card-text">[太原]蔡依林2016PLAY世界巡回演唱会</p>
                </div>
            </div>
        </div>
    </div>
</div>
```

## 9.4、 广告位

![image-20221014105342679](Bootstrap笔记.assets/image-20221014105342679.png)

## 9.5、 历历在目

![image-20221014105411715](Bootstrap笔记.assets/image-20221014105411715.png)

![image-20221014105425386](Bootstrap笔记.assets/image-20221014105425386.png)

## 9.6、 场馆模块

![image-20221014105453591](Bootstrap笔记.assets/image-20221014105453591.png)

![image-20221014105506739](Bootstrap笔记.assets/image-20221014105506739.png)

![image-20221014105530426](Bootstrap笔记.assets/image-20221014105530426.png)

![image-20221014105538254](Bootstrap笔记.assets/image-20221014105538254.png)

![image-20221014112149401](Bootstrap笔记.assets/image-20221014112149401.png)

## 9.7 底部模块

![image-20221014115022116](Bootstrap笔记.assets/image-20221014115022116.png)
